package cmd

import (
	"fmt"
	"os"
	"strconv"

	"github.com/op/go-logging"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	maps "golang.org/x/exp/maps"
)

var (
	cfgFile      string
	rmqUser      string
	rmqPassword  string
	rmqHost      string
	rmqPort      int
	rmqApiPort   int
	rmqVhost     string
	rmqCrt       string
	rmqKey       string
	rmqCa        string
	rmqApiCrt    string
	rmqApiKey    string
	rmqApiCa     string
	rmqProtocol  string
	configMap    map[string]interface{}
	argConfigMap map[string]interface{}
	logger       = logging.MustGetLogger("rmqcli")
	logFile      string
	logBackend   string
	logLevel     string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "rmqcli",
	Short: "RabbitMQ CLI in Go",
	Long:  `RabbitMQ CLI application written in Go.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	argConfigMap = make(map[string]interface{})
	configMap = make(map[string]interface{})

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.rmqcli.yaml)")
	rootCmd.PersistentFlags().StringVar(&rmqUser, "user", "guest", "RabbitMQ user (default is guest)")
	rootCmd.PersistentFlags().StringVar(&rmqPassword, "password", "guest", "RabbitMQ password (default is guest)")
	rootCmd.PersistentFlags().StringVar(&rmqHost, "host", "localhost", "RabbitMQ host (default is localhost)")
	rootCmd.PersistentFlags().IntVar(&rmqPort, "port", 5672, "RabbitMQ port (default is 5672 AMQP)")
	rootCmd.PersistentFlags().IntVar(&rmqApiPort, "apiPort", 15672, "RabbitMQ API port (default is 15672 HTTP)")
	rootCmd.PersistentFlags().StringVar(&rmqVhost, "vhost", "", "RabbitMQ vhost (default is /)")
	rootCmd.PersistentFlags().StringVar(&logLevel, "logLevel", "DEBUG", "Log Level (default is DEBUG)")
	rootCmd.PersistentFlags().StringVar(&logBackend, "logBackend", "stdout", "Log Output (default is stdout)")
	rootCmd.PersistentFlags().StringVar(&logFile, "logFile", "rmqcli.log", "Log File (default is rmcli.log)")
	rootCmd.PersistentFlags().StringVar(&rmqCrt, "crt", "", "Use TLS certificate for AMQPS  (default is '')")
	rootCmd.PersistentFlags().StringVar(&rmqKey, "key", "", "Use TLS certificate key for AMQPS  (default is '')")
	rootCmd.PersistentFlags().StringVar(&rmqCa, "ca", "", "Use TLS certificate CA for AMQPS  (default is '')")
	rootCmd.PersistentFlags().StringVar(&rmqApiCrt, "apiCrt", "", "Use TLS certificate for AMQPS  (default is '')")
	rootCmd.PersistentFlags().StringVar(&rmqApiKey, "apiKey", "", "Use TLS certificate key for AMQPS  (default is '')")
	rootCmd.PersistentFlags().StringVar(&rmqApiCa, "apiCa", "", "Use TLS certificate CA for AMQPS  (default is '')")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".rmqcli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".rmqcli")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

	// Code Defaults
	configMap["host"] = rmqHost
	configMap["user"] = rmqUser
	configMap["password"] = rmqPassword
	configMap["port"] = rmqPort
	configMap["vhost"] = rmqVhost
	configMap["crt"] = rmqCrt
	configMap["key"] = rmqKey
	configMap["ca"] = rmqCa
	configMap["apiCrt"] = rmqApiCrt
	configMap["apiKey"] = rmqApiKey
	configMap["apiCa"] = rmqApiCa
	configMap["logBackend"] = logBackend
	configMap["logFile"] = logFile
	configMap["logLevel"] = logLevel

	// Get config files defaults
	maps.Copy(configMap, viper.AllSettings())

	// Set arguments only if they are defined
	if rootCmd.Flags().Lookup("host").Changed {
		argConfigMap["host"] = rmqHost
	}
	if rootCmd.Flags().Lookup("user").Changed {
		argConfigMap["user"] = rmqUser
	}
	if rootCmd.Flags().Lookup("password").Changed {
		argConfigMap["password"] = rmqPassword
	}
	if rootCmd.Flags().Lookup("port").Changed {
		argConfigMap["port"] = rmqPort
	}
	if rootCmd.Flags().Lookup("vhost").Changed {
		argConfigMap["vhost"] = rmqVhost
	}
	if rootCmd.Flags().Lookup("crt").Changed {
		argConfigMap["crt"] = rmqCrt
	}
	if rootCmd.Flags().Lookup("key").Changed {
		argConfigMap["key"] = rmqKey
	}
	if rootCmd.Flags().Lookup("ca").Changed {
		argConfigMap["ca"] = rmqCa
	}
	if rootCmd.Flags().Lookup("apiCrt").Changed {
		argConfigMap["apiCrt"] = rmqApiCrt
	}
	if rootCmd.Flags().Lookup("apiKey").Changed {
		argConfigMap["apiKey"] = rmqApiKey
	}
	if rootCmd.Flags().Lookup("apiCa").Changed {
		argConfigMap["apiCa"] = rmqApiCa
	}
	if rootCmd.Flags().Lookup("logBackend").Changed {
		argConfigMap["logBackend"] = logBackend
	}
	if rootCmd.Flags().Lookup("logFile").Changed {
		argConfigMap["logFile"] = logFile
	}

	maps.Copy(configMap, argConfigMap)

	if configMap["tls"].(bool) {
		configMap["protocol"] = "amqps"
	} else {
		configMap["protocol"] = "amqp"
	}

	if configMap["user"] != "" && configMap["password"] != "" {
		configMap["auth"] = fmt.Sprintf("%s:%s@", rmqUser, rmqPassword)
	}

	configMap["uri"] = fmt.Sprintf("%s://%s%s:%s/%s", configMap["protocol"], configMap["auth"], configMap["host"], strconv.Itoa(configMap["port"].(int)), configMap["vhost"])

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)

	var loggerBackend logging.Backend
	var err error
	switch configMap["logBackend"] {
	case "syslog":
		loggerBackend, err = logging.NewSyslogBackend("localhost")
		if err != nil {
			panic(err)
		}
	case "file":
		file, err := os.OpenFile(configMap["logFile"].(string), os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
		if err != nil {
			panic(err)
		}
		loggerBackend = logging.NewLogBackend(file, "", 0)
	default:
		loggerBackend = logging.NewLogBackend(os.Stdout, "", 0)
	}

	backendFormated := logging.NewBackendFormatter(loggerBackend, format)
	backendLeveled := logging.AddModuleLevel(backendFormated)

	switch configMap["logLevel"].(string) {
	case "ERROR":
		backendLeveled.SetLevel(logging.ERROR, "")
	case "INFO":
		backendLeveled.SetLevel(logging.INFO, "")
	case "CRITICAL":
		backendLeveled.SetLevel(logging.CRITICAL, "")
	case "NOTICE":
		backendLeveled.SetLevel(logging.NOTICE, "")
	case "WARNING":
		backendLeveled.SetLevel(logging.WARNING, "")
	default:
		backendLeveled.SetLevel(logging.DEBUG, "")
	}

	logging.SetBackend(backendLeveled)

}
