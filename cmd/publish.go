package cmd

import (
	"context"
    amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/cobra"
	"rmqcli/lib"
)

var ( 
	dstQueue string
    dstQueueType string
	dstExchange string
	dstExchangeType string
	routingKey string
    body string
	continuous bool
)

var publishCmd = &cobra.Command{
	Use:   "publish",
	Short: "This command will publish a message into RabbitMQ",
	Long:  `This publish command will publish a message into a queue or exchange into the specified RabbitMQ.`,
	Run: func(cmd *cobra.Command, args []string) {
		//var conn *amqp.Connection
		//var ch *amqp.Channel
		//var err error
		
		exitCh := make(chan struct{})
		confirmsCh := make(chan *amqp.DeferredConfirmation)
		confirmsDoneCh := make(chan struct{})
		// Note: this is a buffered channel so that indicating OK to
		// publish does not block the confirm handler
		publishOkCh := make(chan struct{}, 1)

		lib.SetupCloseHandler(exitCh, logger)

		lib.StartConfirmHandler(publishOkCh, confirmsCh, confirmsDoneCh, exitCh, logger)

		lib.Publish(context.Background(), publishOkCh, confirmsCh, confirmsDoneCh, exitCh, configMap["uri"].(string), configMap["vhost"].(string), configMap["cert"].(string), configMap["key"].(string), configMap["ca"].(string), dstExchange, dstExchangeType, dstQueue, dstQueueType, body, routingKey, continuous ,logger)
	},
}

func init() {
	publishCmd.Flags().StringVar(&body, "body", "", "Message to send (default is '')")
	publishCmd.Flags().StringVar(&dstQueue, "dstQueue", "testQueue", "Destination Queue to send (default is 'testQueue')")
	publishCmd.Flags().StringVar(&dstQueueType, "dstQueueType", "classic", "Destination Queue Type (default is classic)")
	publishCmd.Flags().StringVar(&dstExchange, "dstExchange", "testExchange", "Destination Exchange to send (default is 'testExchange')")
	publishCmd.Flags().StringVar(&dstExchangeType, "dstExchangeType", "direct", "Destination Exchange Type (default is direct)")
	publishCmd.Flags().StringVar(&routingKey, "routingKey", "testRoutingKey", "Routing Key (default is testRoutingKey)")
	publishCmd.Flags().BoolVar(&continuous, "continuous", false, "Continuous Delivery messages (default is false)")
	rootCmd.AddCommand(publishCmd)
}