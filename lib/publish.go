package lib

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/op/go-logging"
	amqp "github.com/rabbitmq/amqp091-go"
)

func SetupCloseHandler(exitCh chan struct{}, logger *logging.Logger) {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		logger.Debugf("close handler: Ctrl+C pressed in Terminal")
		close(exitCh)
	}()
}

func Publish(ctx context.Context, publishOkCh <-chan struct{}, confirmsCh chan<- *amqp.DeferredConfirmation, confirmsDoneCh <-chan struct{}, exitCh chan struct{}, uri string, vhost string, certPath string, keyPath string, caPath string, dstExchange string, dstExchangeType string, dstQueue string, dstQueueType string, body string, routingKey string, continuous bool, logger *logging.Logger) {
	logger.Debugf("producer: dialing %s", uri)
	conn := ConnectToRabbitMQ(uri, vhost, certPath, keyPath, caPath, logger)

	logger.Debug("producer: got Connection, getting Channel")
	channel := ChannelToRabbitMQ(conn, logger)

	logger.Debugf("producer: declaring exchange")
	if err := channel.ExchangeDeclare(
		dstExchange,     // name
		dstExchangeType, // type
		true,            // durable
		false,           // auto-delete
		false,           // internal
		false,           // noWait
		nil,             // arguments
	); err != nil {
		logger.Fatalf("producer: Exchange Declare: %s", err)
	}

	logger.Debugf("producer: declaring queue '%s'", dstQueue)
	queue, err := channel.QueueDeclare(
		dstQueue, // name of the queue
		true,     // durable
		false,    // delete when unused
		false,    // exclusive
		false,    // noWait
		nil,      // arguments
	)
	if err == nil {
		logger.Debugf("producer: declared queue (%q %d messages, %d consumers), binding to Exchange (key %q)",
			queue.Name, queue.Messages, queue.Consumers, routingKey)
	} else {
		logger.Fatalf("producer: Queue Declare: %s", err)
	}

	logger.Debugf("producer: declaring binding")
	if err := channel.QueueBind(queue.Name, routingKey, dstExchange, false, nil); err != nil {
		logger.Fatalf("producer: Queue Bind: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	logger.Debugf("producer: enabling publisher confirms.")
	if err := channel.Confirm(false); err != nil {
		logger.Fatalf("producer: channel could not be put into confirm mode: %s", err)
	}

	for {
		canPublish := false
		logger.Debug("producer: waiting on the OK to publish...")
		for {
			select {
			case <-confirmsDoneCh:
				logger.Debug("producer: stopping, all confirms seen")
				return
			case <-publishOkCh:
				logger.Debug("producer: got the OK to publish")
				canPublish = true
				break
			case <-time.After(time.Second):
				logger.Debug("producer: still waiting on the OK to publish...")
				continue
			}
			if canPublish {
				break
			}
		}

		logger.Debugf("producer: publishing %dB body (%q)", len(body), body)
		dConfirmation, err := channel.PublishWithDeferredConfirmWithContext(
			ctx,
			dstExchange,
			routingKey,
			true,
			false,
			amqp.Publishing{
				Headers:         amqp.Table{},
				ContentType:     "text/plain",
				ContentEncoding: "",
				DeliveryMode:    amqp.Persistent,
				Priority:        0,
				AppId:           "sequential-producer",
				Body:            []byte(body),
			},
		)
		if err != nil {
			logger.Fatalf("producer: error in publish: %s", err)
		}

		select {
		case <-confirmsDoneCh:
			logger.Debug("producer: stopping, all confirms seen")
			return
		case confirmsCh <- dConfirmation:
			logger.Debug("producer: delivered deferred confirm to handler")
			break
		}

		select {
		case <-confirmsDoneCh:
			logger.Debug("producer: stopping, all confirms seen")
			return
		case <-time.After(time.Millisecond * 250):
			if continuous {
				continue
			} else {
				logger.Debug("producer: initiating stop")
				close(exitCh)
				select {
				case <-confirmsDoneCh:
					logger.Debug("producer: stopping, all confirms seen")
					return
				case <-time.After(time.Second * 10):
					logger.Debug("producer: may be stopping with outstanding confirmations")
					return
				}
			}
		}
	}
}

func StartConfirmHandler(publishOkCh chan<- struct{}, confirmsCh <-chan *amqp.DeferredConfirmation, confirmsDoneCh chan struct{}, exitCh <-chan struct{}, logger *logging.Logger) {
	go func() {
		confirms := make(map[uint64]*amqp.DeferredConfirmation)

		for {
			select {
			case <-exitCh:
				ExitConfirmHandler(confirms, confirmsDoneCh, logger)
				return
			default:
				break
			}

			outstandingConfirmationCount := len(confirms)

			// Note: 8 is arbitrary, you may wish to allow more outstanding confirms before blocking publish
			if outstandingConfirmationCount <= 8 {
				select {
				case publishOkCh <- struct{}{}:
					logger.Debug("confirm handler: sent OK to publish")
				case <-time.After(time.Second * 5):
					logger.Debug("confirm handler: timeout indicating OK to publish (this should never happen!)")
				}
			} else {
				logger.Debugf("confirm handler: waiting on %d outstanding confirmations, blocking publish", outstandingConfirmationCount)
			}

			select {
			case confirmation := <-confirmsCh:
				dtag := confirmation.DeliveryTag
				confirms[dtag] = confirmation
			case <-exitCh:
				ExitConfirmHandler(confirms, confirmsDoneCh, logger)
				return
			}

			CheckConfirmations(confirms, logger)
		}
	}()
}

func ExitConfirmHandler(confirms map[uint64]*amqp.DeferredConfirmation, confirmsDoneCh chan struct{}, logger *logging.Logger) {
	logger.Debug("confirm handler: exit requested")
	WaitConfirmations(confirms, logger)
	close(confirmsDoneCh)
	logger.Debug("confirm handler: exiting")
}

func CheckConfirmations(confirms map[uint64]*amqp.DeferredConfirmation, logger *logging.Logger) {
	logger.Debugf("confirm handler: checking %d outstanding confirmations", len(confirms))
	for k, v := range confirms {
		if v.Acked() {
			logger.Debugf("confirm handler: confirmed delivery with tag: %d", k)
			delete(confirms, k)
		}
	}
}

func WaitConfirmations(confirms map[uint64]*amqp.DeferredConfirmation, logger *logging.Logger) {
	logger.Debugf("confirm handler: waiting on %d outstanding confirmations", len(confirms))

	CheckConfirmations(confirms, logger)

	for k, v := range confirms {
		select {
		case <-v.Done():
			logger.Debugf("confirm handler: confirmed delivery with tag: %d", k)
			delete(confirms, k)
		case <-time.After(time.Second):
			logger.Debugf("confirm handler: did not receive confirmation for tag %d", k)
		}
	}

	outstandingConfirmationCount := len(confirms)
	if outstandingConfirmationCount > 0 {
		logger.Debugf("confirm handler: exiting with %d outstanding confirmations", outstandingConfirmationCount)
	} else {
		logger.Debug("confirm handler: done waiting on outstanding confirmations")
	}
}
