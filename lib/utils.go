package lib

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	"github.com/op/go-logging"
	amqp "github.com/rabbitmq/amqp091-go"
)

func ConnectToRabbitMQ(uri string, vhost string, certPath string, keyPath string, caPath string, logger *logging.Logger) *amqp.Connection {
	var config amqp.Config
	var tlsConf *tls.Config

	if certPath != "" && caPath != "" {
		// Create a certificate pool from the root CA certificate.
		rootCAs := x509.NewCertPool()
		pemBytes, err := ioutil.ReadFile(caPath)
		if err != nil {
			logger.Fatalf("Failed to load root CA certificate: %s", caPath)
			panic("Failed to load root CA certificate")
		}
		rootCAs.AppendCertsFromPEM(pemBytes)

		// Create a certificate from the client certificate and key.
		cert, err := tls.LoadX509KeyPair(certPath, keyPath)
		if err != nil {
			logger.Fatalf("Failed to load client certificate: cert: %s key: %s", certPath, keyPath)
			panic("Failed to load client certificate")
		}

		// Create a TLS configuration object.
		tlsConf = &tls.Config{
			RootCAs:      rootCAs,
			Certificates: []tls.Certificate{cert},
		}

	}

	if tlsConf != nil {
		config = amqp.Config{
			Vhost:           vhost,
			Properties:      amqp.NewConnectionProperties(),
			TLSClientConfig: tlsConf,
		}
	} else {
		config = amqp.Config{
			Vhost:      vhost,
			Properties: amqp.NewConnectionProperties(),
		}
	}
	config.Properties.SetClientConnectionName("producer-with-confirms")
	// Connect to the RabbitMQ server.
	conn, err := amqp.DialConfig(uri, config)

	if err != nil {
		logger.Fatalf("producer: error in dial: %s", err)
		defer conn.Close()
		return nil
	}

	return conn
}

func ChannelToRabbitMQ(conn *amqp.Connection, logger *logging.Logger) *amqp.Channel {
	// Create a channel.
	ch, err := conn.Channel()
	if err != nil {
		logger.Fatalf("error getting a channel: %s", err)
		defer ch.Close()
		return nil
	}

	return ch
}
