package main

import (
	"rmqcli/cmd"
	"github.com/op/go-logging"
)

var logger = logging.MustGetLogger("rmqcli")

func main() {
	cmd.Execute()
}
